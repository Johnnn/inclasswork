/**********************************************
* File: fib.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Contains the main driver function for a Fibonacci
* solver 
**********************************************/

#include <iostream>
#include "Supp.h"
#include "Recurse.h"

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
* 
* Main driver function for Fibonacci solver 
********************************************/
int main(int argc, char **argv){

	std::cout << Fib(getArgv1Num(argc, argv)) << std::endl;

	return 0;
}
